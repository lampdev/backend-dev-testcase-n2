<?php


namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="client_discr", type="string")
 * @ORM\Table(name="client_matches_your_fake")
 */
class ClientMatchesYourFake extends ClientMatch
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(name="client_match_id", type="integer")
     */
    protected $clientMatchId;

    /**
     * ClientMatch constructor.
     * @param $clientMatchId
     */
    public function __construct(
        $clientMatchId
    ) {
        $this->setClientMatchId($clientMatchId);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $clientMatchId
     */
    public function setClientMatchId($clientMatchId)
    {
        $this->clientMatchId = $clientMatchId;
    }

    /**
     * @return mixed
     */
    public function getClientMatchId()
    {
        return $this->clientMatchId;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'service_match_id' => $this->getClientMatchId()
        ];
    }
}
