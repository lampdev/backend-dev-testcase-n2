<?php

return [
    'importers' => [
        [
            'name' => 'SomeClient',
            'codename' => 'SomeClientCodename',
            'delay' => 30,
            'entity' => App\Entities\ClientMatchesYourFake::class,
            'automapping' => true,
            'parser' => [
                'fetcher' => App\Fetchers\CustomFetcher::class,
                'endpoint' => env('SOMECLIENT_ENDPOINT', 'http://localhost:8000'),
                'path' => '/custom-fetcher-service'
            ]
        ]
    ],
    'command' => "php ".__DIR__."/../artisan custom-client-matches:import --source=%s"
];
