<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\DB;

class DatabaseSeederTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDatabaseSeeding()
    {
        $this->artisan('db:seed');
        $tableRostersCount = count(DB::table('rosters')->get());
        $this->assertEquals($tableRostersCount, env('TEST_DATA_COUNT'));
        $tableMatchesCount = count(DB::table('matches')->get());
        $this->assertEquals($tableMatchesCount, env('TEST_DATA_COUNT'));
        $tableMatchOpponentsCount = count(DB::table('match_opponents')->get());
        $this->assertEquals($tableMatchOpponentsCount, env('TEST_DATA_COUNT'));
    }
}
