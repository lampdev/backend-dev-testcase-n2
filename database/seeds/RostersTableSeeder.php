<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RostersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= env('TEST_DATA_COUNT'); $i++) {
            DB::table('rosters')->insert([
                'team_name' => 'Testname ' . $i,
                'created_at' => date("Y-m-d H:i:s")
            ]);
        }
    }
}
