<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MatchesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= env('TEST_DATA_COUNT'); $i++) {
            DB::table('matches')->insert([
                'winner_id' => rand(1, env('TEST_DATA_COUNT')),
                'start' => date("Y-m-d H:i:s"),
                'status' => rand(0, 255),
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ]);
        }
    }
}
