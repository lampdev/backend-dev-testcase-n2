<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MatchOpponentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= env('TEST_DATA_COUNT'); $i++) {
            DB::table('match_opponents')->insert([
                'roster_id' => $i,
                'match_id' => $i
            ]);
        }
    }
}
